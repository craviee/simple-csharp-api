using System.Collections.Generic;
using System.Linq;
using api.Dtos;
using api.Models;
using api.Services;
using Microsoft.AspNetCore.Mvc;

namespace api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReviewerController : Controller
    {
        private readonly IReviewerRepository _reviewerRepository;
        private readonly IBookRepository _bookRepository;
        private readonly IReviewRepository _reviewRepository;

        public ReviewerController(IReviewerRepository reviewerRepository, IBookRepository bookRepository, IReviewRepository reviewRepository)
        {
            _reviewerRepository = reviewerRepository;
            _bookRepository = bookRepository;
            _reviewRepository = reviewRepository;
        }

        [HttpGet]
        [ProducesResponseType(400)]
        [ProducesResponseType(200, Type = typeof(IEnumerable<ReviewerDto>))]
        public IActionResult GetReviewers()
        {
            var reviewers = _reviewerRepository.GetReviewers().ToList();

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var personDto = reviewers.Select(r => new ReviewerDto(r.Id, r.FirstName, r.LastName)).ToList();
            return Ok(personDto);
        }

        [HttpGet("{reviewerId:int}", Name = "GetReviewer")]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(200, Type = typeof(ReviewerDto))]
        public IActionResult GetReviewer(int reviewerId)
        {
            if (!_reviewerRepository.ReviewerExistsById(reviewerId))
                return NotFound();

            var reviewer = _reviewerRepository.GetReviewer(reviewerId);

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var personDto = new ReviewerDto(reviewer.Id, reviewer.FirstName, reviewer.LastName);
            return Ok(personDto);
        }

        [HttpGet("book/{bookId:int}")]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(200, Type = typeof(IEnumerable<ReviewerDto>))]
        public IActionResult GetReviewersByBook(int bookId)
        {
            if (!_bookRepository.BookExistsById(bookId))
                return NotFound();

            var reviewers = _reviewerRepository.GetReviewersByBook(bookId);

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var personDto = reviewers.Select(r => new ReviewerDto(r.Id, r.FirstName, r.LastName)).ToList();
            return Ok(personDto);
        }

        [HttpGet("review/{reviewId:int}")]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(200, Type = typeof(ReviewerDto))]
        public IActionResult GetReviewerByReview(int reviewId)
        {
            if (!_reviewRepository.ReviewExists(reviewId))
                return NotFound();

            var reviewer = _reviewerRepository.GetReviewerByReview(reviewId);

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var personDto = new ReviewerDto(reviewer.Id, reviewer.FirstName, reviewer.LastName);
            return Ok(personDto);
        }


        [HttpPost]
        [ProducesResponseType(200, Type = typeof(ReviewerDto))]
        [ProducesResponseType(500)]
        [ProducesResponseType(422)]
        [ProducesResponseType(400)]
        public IActionResult CreateReviewer([FromBody] Reviewer newReviewer)
        {
            if (newReviewer == null) return BadRequest(ModelState);
            if (_reviewerRepository.ReviewerExistsByName(newReviewer.FirstName, newReviewer.LastName))
            {
                ModelState.AddModelError("", $"The reviewer {newReviewer.FirstName} {newReviewer.LastName} already exists in the database");
                return StatusCode(422, ModelState);
            }
            if (!ModelState.IsValid) return BadRequest(ModelState);
            newReviewer.Id = 0;
            if (_reviewerRepository.CreateReviewer(newReviewer)) return CreatedAtRoute("GetReviewer", new {reviewerId = newReviewer.Id}, newReviewer);
            ModelState.AddModelError("", $"Something went wrong on creating the reviewer {newReviewer.FirstName} {newReviewer.LastName}");
            return StatusCode(500, ModelState);
        }

        [HttpPut("{reviewerId:int}")]
        [ProducesResponseType(200, Type = typeof(ReviewerDto))]
        [ProducesResponseType(500)]
        [ProducesResponseType(400)]
        [ProducesResponseType(422)]
        [ProducesResponseType(404)]
        public IActionResult UpdateReviewer(int reviewerId, [FromBody] Reviewer updatedReviewer)
        {
            if (updatedReviewer == null || reviewerId != updatedReviewer.Id) return BadRequest(ModelState);
            if (!_reviewerRepository.ReviewerExistsById(reviewerId)) return NotFound();
            if (_reviewerRepository.ReviewerExistsByName(updatedReviewer.FirstName, updatedReviewer.LastName))
            {
                ModelState.AddModelError("", $"There is already a reviewer with the name {updatedReviewer.FirstName} {updatedReviewer.LastName} in the database");
                return StatusCode(422, ModelState);
            }
            if (!ModelState.IsValid) return BadRequest(ModelState);
            if (_reviewerRepository.UpdateReviewer(updatedReviewer)) return CreatedAtRoute("GetReviewer", new {reviewerId = reviewerId}, updatedReviewer);
            ModelState.AddModelError("", $"Something went wrong on updating the reviewer with Id {reviewerId}");
            return StatusCode(500, ModelState);
        }

        [HttpDelete("{reviewerId:int}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        [ProducesResponseType(409)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public IActionResult DeleteReviewer(int reviewerId)
        {
            if (!_reviewerRepository.ReviewerExistsById(reviewerId)) return NotFound();
            if (_reviewRepository.GetReviewsByReviewer(reviewerId).Count != 0)
            {
                ModelState.AddModelError("", $"There is at least one review of the reviewer with id {reviewerId}");
                return StatusCode(409, ModelState);
            }

            if (!ModelState.IsValid) return BadRequest();
            if (_reviewerRepository.DeleteReviewer(reviewerId)) return NoContent();
            ModelState.AddModelError("", $"Something went wrong on deleting the reviewer with Id {reviewerId}");
            return StatusCode(500, ModelState);
        }
    }

}