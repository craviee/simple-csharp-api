using System.Collections.Generic;
using System.Linq;
using api.Dtos;
using api.Models;
using api.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking.Internal;

namespace api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CountryController : Controller
    {
        private ICountryRepository _countryRepository;
        private IAuthorRepository _authorRepository;

        public CountryController(ICountryRepository countryRepository, IAuthorRepository authorRepository)
        {
            _countryRepository = countryRepository;
            _authorRepository = authorRepository;
        }

        [HttpGet]
        [ProducesResponseType(400)]
        [ProducesResponseType(200, Type = typeof(IEnumerable<CountryDto>))]
        public IActionResult GetCountries()
        {
            var countries = _countryRepository.GetCountries().ToList();

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var countriesDto = countries.Select(c => new CountryDto(c.Id, c.Name)).ToList();
            return Ok(countriesDto);
        }

        [HttpGet("{countryId:int}", Name = "GetCountry")]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(200, Type = typeof(CountryDto))]
        public IActionResult GetCountry(int countryId)
        {
            if (!_countryRepository.CountryExistsById(countryId))
                return NotFound();

            var country = _countryRepository.GetCountry(countryId);

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var countryDto = new CountryDto(country.Id, country.Name);
            return Ok(countryDto);
        }

        [HttpGet("authors/{authorId:int}")]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(200, Type = typeof(CountryDto))]
        public IActionResult GetCountryOfAnAuthor(int authorId)
        {
            if (!_authorRepository.AuthorExistsById(authorId))
                return NotFound();

            var country = _countryRepository.GetCountryOfAnAuthor(authorId);

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var countryDto = new CountryDto(country.Id, country.Name);
            return Ok(countryDto);
        }

        [HttpPost]
        [ProducesResponseType(400)]
        [ProducesResponseType(422)]
        [ProducesResponseType(500)]
        [ProducesResponseType(200, Type = typeof(CountryDto))]
        public IActionResult CreateCountry([FromBody]Country newCountry)
        {
            if (newCountry == null)
                return BadRequest(ModelState);

            if (_countryRepository.CountryExistsByName(newCountry.Name))
            {
                ModelState.AddModelError("", $"Country {newCountry.Name} already exists");
                return StatusCode(422, ModelState);
            }

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            newCountry.Id = 0;

            if (_countryRepository.CreateCountry(newCountry)) return CreatedAtRoute("GetCountry", new {countryId = newCountry.Id}, newCountry);
            ModelState.AddModelError("", $"Something went wrong saving {newCountry.Name}");
            return StatusCode(500, ModelState);
        }

        [HttpPut("{countryId:int}")]
        [ProducesResponseType(200, Type = typeof(CountryDto))]
        [ProducesResponseType(500)]
        [ProducesResponseType(400)]
        [ProducesResponseType(422)]
        [ProducesResponseType(404)]
        public IActionResult UpdateCountry(int countryId, [FromBody] Country updatedCountry)
        {
            if (updatedCountry == null || updatedCountry.Id != countryId) return BadRequest(ModelState);

            if (!_countryRepository.CountryExistsById(updatedCountry.Id))
            {
                ModelState.AddModelError("", $"The country Id {updatedCountry.Id} is not in the database");
                return NotFound(ModelState);
            }

            if (_countryRepository.CountryExistsByName(updatedCountry.Name))
            {
                ModelState.AddModelError("", $"Country named {updatedCountry.Name} already exists");
                return StatusCode(422, ModelState);
            }

            if (!ModelState.IsValid) return BadRequest(ModelState);

            if (_countryRepository.UpdateCountry(updatedCountry)) return CreatedAtRoute("GetCountry", new {countryId = updatedCountry.Id}, updatedCountry);
            ModelState.AddModelError("",$"Something went wrong on updating the country with Id {updatedCountry.Id}");
            return StatusCode(500, ModelState);
        }

        [HttpDelete("{countryId:int}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(500)]
        [ProducesResponseType(404)]
        [ProducesResponseType(400)]
        [ProducesResponseType(409)]
        public IActionResult DeleteCountry(int countryId)
        {
            if (!_countryRepository.CountryExistsById(countryId)) return NotFound();

            if (_authorRepository.GetAuthorsFromACountry(countryId).Count != 0)
            {
                ModelState.AddModelError("", $"There are Authors linked with the country with Id {countryId}");
                return StatusCode(409,ModelState);
            }

            if (!ModelState.IsValid) return BadRequest(ModelState);

            if (_countryRepository.DeleteCountry(countryId)) return NoContent();

            ModelState.AddModelError("", $"Something went wrong on deleting country with Id {countryId}");
            return StatusCode(500, ModelState);
        }
    }
}