using System;
using System.Collections.Generic;
using System.Linq;
using api.Dtos;
using api.Models;
using api.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorController : Controller
    {
        private readonly IAuthorRepository _authorRepository;
        private readonly IBookRepository _bookRepository;
        private readonly ICountryRepository _countryRepository;

        public AuthorController(IAuthorRepository authorRepository, ICountryRepository countryRepository, IBookRepository bookRepository)
        {
            _authorRepository = authorRepository;
            _countryRepository = countryRepository;
            _bookRepository = bookRepository;
        }

        [HttpGet]
        [ProducesResponseType(200, Type = typeof(IEnumerable<AuthorDto>))]
        [ProducesResponseType(400)]
        public IActionResult GetAuthors()
        {
            var authors = _authorRepository.GetAuthors();

            if (!ModelState.IsValid)
                return BadRequest();
            
            var authorsDto = authors.Select(a => new AuthorDto(a.Id, a.FirstName, a.LastName, new CountryDto(a.Country.Id, a.Country.Name)));
            return Ok(authorsDto);
        }

        [HttpGet("{authorId:int}", Name = "GetAuthor")]
        [ProducesResponseType(200, Type = typeof(AuthorDto))]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public IActionResult GetAuthor(int authorId)
        {
            if (!_authorRepository.AuthorExistsById(authorId))
                return NotFound();

            var author = _authorRepository.GetAuthor(authorId);

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var authorDto = new AuthorDto(author.Id, author.FirstName, author.LastName, new CountryDto(author.Country.Id, author.Country.Name));
            return Ok(authorDto);
        }

        [HttpGet("book/{bookId:int}")]
        [ProducesResponseType(200, Type = typeof(IEnumerable<AuthorDto>))]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public IActionResult GetAuthorsByBook(int bookId)
        {
            if (!_bookRepository.BookExistsById(bookId))
                return NotFound();

            var authors = _authorRepository.GetAuthorsOfABook(bookId);

            if (!ModelState.IsValid)
                return BadRequest();

            var authorsDto = authors.Select(a => new AuthorDto(a.Id, a.FirstName, a.LastName, new CountryDto(a.Country.Id, a.Country.Name)));
            return Ok(authorsDto);
        }

        [HttpGet("country/{countryId:int}")]
        [ProducesResponseType(200, Type = typeof(IEnumerable<AuthorDto>))]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public IActionResult GetAuthorsByCountry(int countryId)
        {
            if (!_countryRepository.CountryExistsById(countryId))
                return NotFound();

            var authors = _authorRepository.GetAuthorsFromACountry(countryId);

            if (!ModelState.IsValid)
                return BadRequest();

            var authorsDto = authors.Select(a => new AuthorDto(a.Id, a.FirstName, a.LastName, new CountryDto(a.Country.Id, a.Country.Name)));
            return Ok(authorsDto);
        }

        [HttpPost]
        [ProducesResponseType(200, Type = typeof(AuthorDto))]
        [ProducesResponseType(400)]
        [ProducesResponseType(422)]
        [ProducesResponseType(500)]
        public IActionResult CreateAuthor([FromBody] Author newAuthor)
        {
            if (newAuthor == null) return BadRequest(ModelState);
            if (_authorRepository.AuthorExistsByName(newAuthor.FirstName, newAuthor.LastName))
            {
                ModelState.AddModelError("", $"The author {newAuthor.FirstName} {newAuthor.LastName} is already created in the database");
                return StatusCode(422, ModelState);
            }

            if (newAuthor.Country == null)
            {
                ModelState.AddModelError("", $"There is no country assigned to this author");
                return BadRequest(ModelState);
            }

            if (!_countryRepository.CountryExistsById(newAuthor.Country.Id))
            {
                ModelState.AddModelError("", $"There is no country with Id {newAuthor.Country.Id}");
                return NotFound();
            }

            newAuthor.Country = _countryRepository.GetCountry(newAuthor.Country.Id);

            newAuthor.Id = 0;

            if (!ModelState.IsValid) return BadRequest(ModelState);
            if (_authorRepository.CreateAuthor(newAuthor)) return CreatedAtRoute("GetAuthor", new {authorId = newAuthor.Id}, newAuthor);
            ModelState.AddModelError("", $"Something went wrong in creating the author named {newAuthor.FirstName} {newAuthor.LastName}");
            return StatusCode(500, ModelState);
        }

        [HttpPut("{authorId:int}")]
        [ProducesResponseType(200, Type = typeof(AuthorDto))]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(422)]
        [ProducesResponseType(500)]
        public IActionResult UpdateAuthor(int authorId, [FromBody] Author updatedAuthor)
        {
            if (updatedAuthor == null || authorId != updatedAuthor.Id) return BadRequest(ModelState);
            if (!_authorRepository.AuthorExistsById(authorId)) return NotFound();
            if (_authorRepository.AuthorDuplicatedByName(updatedAuthor.Id, updatedAuthor.FirstName, updatedAuthor.LastName))
            {
                ModelState.AddModelError("", $"There is already an author named {updatedAuthor.FirstName} {updatedAuthor.LastName}");
                return StatusCode(422, ModelState);
            }
            if (updatedAuthor.Country == null)
            {
                ModelState.AddModelError("", $"There is no country assigned to this author");
                return BadRequest(ModelState);
            }

            if (!_countryRepository.CountryExistsById(updatedAuthor.Country.Id))
            {
                ModelState.AddModelError("", $"There is no country with Id {updatedAuthor.Country.Id}");
                return NotFound();
            }

            updatedAuthor.Country = _countryRepository.GetCountry(updatedAuthor.Country.Id);
            if (!ModelState.IsValid) return BadRequest(ModelState);
            if (_authorRepository.UpdateAuthor(updatedAuthor)) return CreatedAtRoute("GetAuthor", new {authorId = authorId}, updatedAuthor);
            ModelState.AddModelError("", $"Something went wrong on updating the author with Id {authorId}");
            return StatusCode(500, ModelState);
        }

        [HttpDelete("{authorId:int}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        [ProducesResponseType(409)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public IActionResult DeleteAuthor(int authorId)
        {
            if (!_authorRepository.AuthorExistsById(authorId)) return NotFound();
            if (_bookRepository.GetBooksOfAnAuthor(authorId).Count != 0)
            {
                ModelState.AddModelError("", $"There is at least one book with the author with Id {authorId}");
                return StatusCode(409, ModelState);
            }
            if (!ModelState.IsValid) return BadRequest(ModelState);
            if (_authorRepository.DeleteAuthor(authorId)) return NoContent();
            ModelState.AddModelError("", $"Something went wrong on deleting the author with Id {authorId}");
            return StatusCode(500, ModelState);
        }
    }
}