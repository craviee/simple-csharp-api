using System.Collections.Generic;
using System.Linq;
using api.Dtos;
using api.Models;
using api.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : Controller
    {
        private ICategoryRepository _categoryRepository;
        private IBookRepository _bookRepository;

        public CategoryController(ICategoryRepository categoryRepository, IBookRepository bookRepository)
        {
            _categoryRepository = categoryRepository;
            _bookRepository = bookRepository;
        }

        [HttpGet]
        [ProducesResponseType(400)]
        [ProducesResponseType(200, Type = typeof(IEnumerable<CategoryDto>))]
        public IActionResult GetCategories()
        {
            var categories = _categoryRepository.GetCategories().ToList();

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var categoriesDto = categories.Select(c => new CategoryDto(c.Id, c.Name)).ToList();
            return Ok(categoriesDto);
        }

        [HttpGet("{categoryId:int}", Name = "GetCategory")]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(200, Type = typeof(CategoryDto))]
        public IActionResult GetCategory(int categoryId)
        {
            if (!_categoryRepository.CategoryExistsById(categoryId))
                return NotFound();

            var category = _categoryRepository.GetCategory(categoryId);

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var categoryDto = new CategoryDto(category.Id, category.Name);
            return Ok(categoryDto);

        }

        [HttpGet("book/{bookId:int}")]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(200, Type = typeof(IEnumerable<CategoryDto>))]
        public IActionResult GetCategoriesByBook(int bookId)
        {
            if (!_bookRepository.BookExistsById(bookId))
                return NotFound();

            var categories = _categoryRepository.GetCategoriesByBook(bookId).ToList();

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var categoriesDto = categories.Select(c => new CategoryDto(c.Id, c.Name)).ToList();
            return Ok(categoriesDto);
        }

        [HttpPost]
        [ProducesResponseType(200, Type = typeof(CategoryDto))]
        [ProducesResponseType(500)]
        [ProducesResponseType(400)]
        [ProducesResponseType(422)]
        public IActionResult CreateCategory([FromBody] Category newCategory)
        {
            if (newCategory == null) return BadRequest();

            if (_categoryRepository.CategoryExistsByName(newCategory.Name))
            {
                ModelState.AddModelError("", $"The category named {newCategory.Name} already exists");
                return StatusCode(422, ModelState);
            }

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            newCategory.Id = 0;

            if (_categoryRepository.CreateCategory(newCategory)) return CreatedAtRoute("GetCategory", new {categoryId = newCategory.Id}, newCategory);
            ModelState.AddModelError("", "Something went wrong on creating a new category");
            return StatusCode(500, ModelState);
        }

        [HttpPut("{categoryId:int}")]
        [ProducesResponseType(200, Type = typeof(CategoryDto))]
        [ProducesResponseType(500)]
        [ProducesResponseType(422)]
        [ProducesResponseType(400)]
        public IActionResult UpdateCategory(int categoryId, [FromBody] Category updatedCategory)
        {
            if (!_categoryRepository.CategoryExistsById(categoryId)) return NotFound();
            if (updatedCategory == null || categoryId != updatedCategory.Id) return BadRequest(ModelState);
            if (_categoryRepository.CategoryExistsByName(updatedCategory.Name))
            {
                ModelState.AddModelError("", $"The category named {updatedCategory.Name} already exists");
                return StatusCode(422, ModelState);
            }
            if (!ModelState.IsValid) return BadRequest();
            if (_categoryRepository.UpdateCategory(updatedCategory)) return CreatedAtRoute("GetCategory", new {categoryId = categoryId}, updatedCategory);
            ModelState.AddModelError("", $"Something went on updating the category with Id {categoryId}");
            return StatusCode(500, ModelState);
        }

        [HttpDelete("{categoryId:int}")]
        public IActionResult DeleteCategory(int categoryId)
        {
            if (!_categoryRepository.CategoryExistsById(categoryId)) return NotFound();
            if (_bookRepository.GetBooksByCategory(categoryId).Count > 0)
            {
                ModelState.AddModelError("", $"There is at least one book in this category");
                return StatusCode(409, ModelState);
            }
            if (!ModelState.IsValid) return BadRequest();
            if (_categoryRepository.DeleteCategory(categoryId)) return NoContent();
            ModelState.AddModelError("", $"Something went wrong on deleting the category with Id {categoryId}");
            return StatusCode(500, ModelState);
        }
    }
}