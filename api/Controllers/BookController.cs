using System.Collections;
using System.Collections.Generic;
using System.Linq;
using api.Dtos;
using api.Models;
using api.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookController : Controller
    {
        private readonly IBookRepository _bookRepository;
        private readonly IAuthorRepository _authorRepository;
        private readonly ICategoryRepository _categoryRepository;
        private readonly IReviewRepository _reviewRepository;

        public BookController(IBookRepository bookRepository, IAuthorRepository authorRepository, ICategoryRepository categoryRepository, IReviewRepository reviewRepository)
        {
            _bookRepository = bookRepository;
            _authorRepository = authorRepository;
            _categoryRepository = categoryRepository;
            _reviewRepository = reviewRepository;
        }

        [HttpGet]
        [ProducesResponseType(200, Type = typeof(IEnumerable<BookDto>))]
        [ProducesResponseType(400)]
        public IActionResult GetBooks()
        {
            var books = _bookRepository.GetBooks();

            if (!ModelState.IsValid)
                return BadRequest(ModelState);


            var booksDtos = books.Select(b => new BookDto(b.Id, b.Title, b.Isbn, b.DatePublished,
                b.BookAuthors.Select(b => new AuthorDto(b.AuthorId, b.Author.FirstName, b.Author.LastName, new CountryDto(b.Author.Country.Id, b.Author.Country.Name))).ToList(),
                b.BookCategories.Select(b => new CategoryDto(b.CategoryId, b.Category.Name)).ToList()));
            return Ok(booksDtos);
        }

        [HttpGet("{bookId:int}", Name = "GetBookById")]
        [ProducesResponseType(200, Type = typeof(BookDto))]
        [ProducesResponseType(404)]
        [ProducesResponseType(400)]
        public IActionResult GetBookById(int bookId)
        {
            if (!_bookRepository.BookExistsById(bookId))
                return NotFound();

            var book = _bookRepository.GetBookById(bookId);

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var bookDto = new BookDto(book.Id, book.Title, book.Isbn, book.DatePublished,
                book.BookAuthors.Select(b => new AuthorDto(b.AuthorId, b.Author.FirstName, b.Author.LastName, new CountryDto(b.Author.Country.Id, b.Author.Country.Name))).ToList(),
                book.BookCategories.Select(b => new CategoryDto(b.CategoryId, b.Category.Name)).ToList());
            return Ok(bookDto);
        }

        [HttpGet("isbn/{isbn}")]
        [ProducesResponseType(200, Type = typeof(BookDto))]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public IActionResult GetBookByIsbn(string isbn)
        {
            if (!_bookRepository.BookExistsByIsbn(isbn))
                return NotFound();

            var book = _bookRepository.GetBookByIsbn(isbn);

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var bookDto = new BookDto(book.Id, book.Title, book.Isbn, book.DatePublished,
                book.BookAuthors.Select(b => new AuthorDto(b.AuthorId, b.Author.FirstName, b.Author.LastName, new CountryDto(b.Author.Country.Id, b.Author.Country.Name))).ToList(),
                book.BookCategories.Select(b => new CategoryDto(b.CategoryId, b.Category.Name)).ToList());
            return Ok(bookDto);
        }

        [HttpGet("author/{authorId:int}")]
        [ProducesResponseType(200, Type = typeof(IEnumerable<BookDto>))]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public IActionResult GetBooksByAuthor(int authorId)
        {
            if (!_authorRepository.AuthorExistsById(authorId))
                return NotFound();

            var books = _bookRepository.GetBooksOfAnAuthor(authorId);

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var bookDtos = books.Select(b => new BookDto(b.Id, b.Title, b.Isbn, b.DatePublished,
                b.BookAuthors.Select(b => new AuthorDto(b.AuthorId, b.Author.FirstName, b.Author.LastName, new CountryDto(b.Author.Country.Id, b.Author.Country.Name))).ToList(),
                b.BookCategories.Select(b => new CategoryDto(b.CategoryId, b.Category.Name)).ToList()));
            return Ok(bookDtos);
        }

        [HttpGet("category/{categoryId:int}")]
        [ProducesResponseType(200, Type = typeof(IEnumerable<BookDto>))]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public IActionResult GetBooksByCategory(int categoryId)
        {
            if (!_categoryRepository.CategoryExistsById(categoryId))
                return NotFound();

            var books = _bookRepository.GetBooksByCategory(categoryId);

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var bookDtos = books.Select(b => new BookDto(b.Id, b.Title, b.Isbn, b.DatePublished,
                b.BookAuthors.Select(b => new AuthorDto(b.AuthorId, b.Author.FirstName, b.Author.LastName, new CountryDto(b.Author.Country.Id, b.Author.Country.Name))).ToList(),
                b.BookCategories.Select(b => new CategoryDto(b.CategoryId, b.Category.Name)).ToList()));
            return Ok(bookDtos);
        }

        [HttpGet("review/{reviewId:int}")]
        [ProducesResponseType(200, Type = typeof(BookDto))]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public IActionResult GetBookByReview(int reviewId)
        {
            if (!_reviewRepository.ReviewExists(reviewId))
                return NotFound();

            var book = _bookRepository.GetBookByReview(reviewId);

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var bookDto = new BookDto(book.Id, book.Title, book.Isbn, book.DatePublished,
                book.BookAuthors.Select(b => new AuthorDto(b.AuthorId, b.Author.FirstName, b.Author.LastName, new CountryDto(b.Author.Country.Id, b.Author.Country.Name))).ToList(),
                book.BookCategories.Select(b => new CategoryDto(b.CategoryId, b.Category.Name)).ToList());
            return Ok(bookDto);
        }

        [HttpGet("{bookId:int}/rating")]
        [ProducesResponseType(200, Type = typeof(decimal))]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public IActionResult GetAverageRatingByBook(int bookId)
        {
            if (!_bookRepository.BookExistsById(bookId))
                return NotFound();

            var rating = _bookRepository.GetAverageRatingByBook(bookId);

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            return Ok(rating);
        }

        [HttpPost]
        [ProducesResponseType(200, Type = typeof(BookDto))]
        [ProducesResponseType(422)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public IActionResult CreateBook([FromQuery] List<int> authorId, [FromQuery] List<int> categoryId, [FromBody] Book newBook)
        {
            if (newBook == null) return BadRequest(ModelState);
            if (authorId == null || authorId.Count == 0)
            {
                ModelState.AddModelError("", $"There is no authorId in the query");
                return NotFound(ModelState);
            }
            if (categoryId == null || categoryId.Count == 0)
            {
                ModelState.AddModelError("", $"There is no categoryId in the query");
                return NotFound(ModelState);
            }
            if (_bookRepository.BookExistsByIsbn(newBook.Isbn))
            {
                ModelState.AddModelError("", $"There is already a book with the ISBN {newBook.Isbn}");
                return StatusCode(422, ModelState);
            }

            foreach (var id in categoryId)
            {
                if (_categoryRepository.CategoryExistsById(id)) continue;
                ModelState.AddModelError("", $"There is no category with Id {id}");
                return NotFound(ModelState);
            }
            foreach (var id in authorId)
            {
                if (_authorRepository.AuthorExistsById(id)) continue;
                ModelState.AddModelError("", $"There is no author with Id {id}");
                return NotFound(ModelState);
            }

            if (!ModelState.IsValid) return BadRequest(ModelState);

            newBook.Id = 0;

            if (_bookRepository.CreateBook(authorId, categoryId,newBook)) return CreatedAtRoute("GetBookById", new {bookId = newBook.Id}, newBook);

            ModelState.AddModelError("", $"Something went wrong on creating the book");
            return StatusCode(500, ModelState);
        }

        [HttpPut("{bookId:int}")]
        [ProducesResponseType(200, Type = typeof(BookDto))]
        public IActionResult UpdateBook(int bookId, [FromQuery] List<int> authorId, [FromQuery] List<int> categoryId, [FromBody] Book updatedBook)
        {
            if (updatedBook == null || bookId != updatedBook.Id) return BadRequest(ModelState);
            if (_bookRepository.BookDuplicatedByIsbn(bookId, updatedBook.Isbn))
            {
                ModelState.AddModelError("", $"There is already a book with the Isbn {updatedBook.Isbn}");
                return StatusCode(422, ModelState);
            }
            if (authorId == null || authorId.Count == 0)
            {
                ModelState.AddModelError("", $"There is no authorId in the query");
                return NotFound(ModelState);
            }
            if (categoryId == null || categoryId.Count == 0)
            {
                ModelState.AddModelError("", $"There is no categoryId in the query");
                return NotFound(ModelState);
            }
            foreach (var id in categoryId)
            {
                if (_categoryRepository.CategoryExistsById(id)) continue;
                ModelState.AddModelError("", $"There is no category with Id {id}");
                return NotFound(ModelState);
            }
            foreach (var id in authorId)
            {
                if (_authorRepository.AuthorExistsById(id)) continue;
                ModelState.AddModelError("", $"There is no author with Id {id}");
                return NotFound(ModelState);
            }
            if (!ModelState.IsValid) return BadRequest(ModelState);

            updatedBook.BookAuthors = null;
            updatedBook.BookCategories = null;

            if(_bookRepository.UpdateBook(authorId, categoryId, updatedBook)) return CreatedAtRoute("GetBookById", new {bookId = bookId}, updatedBook);
            ModelState.AddModelError("", $"Something went wrong on updating the book with Id {bookId}");
            return StatusCode(500, ModelState);
        }

        [HttpDelete("{bookId:int}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(500)]
        [ProducesResponseType(409)]
        [ProducesResponseType(404)]
        [ProducesResponseType(400)]
        public IActionResult DeleteBook(int bookId)
        {
            if (!_bookRepository.BookExistsById(bookId))
            {
                ModelState.AddModelError("", $"There is no book with the Id {bookId}");
                return NotFound(ModelState);
            }

            if (_reviewRepository.GetReviewsByBook(bookId).Count != 0)
            {
                ModelState.AddModelError("", $"There is at least one review with the book with Id {bookId}");
                return StatusCode(409, ModelState);
            }

            if (!ModelState.IsValid) return BadRequest(ModelState);

            if (_bookRepository.DeleteBook(bookId)) return NoContent();
            ModelState.AddModelError("", $"Something went wrong on deleting the book with Id {bookId}");
            return StatusCode(500, ModelState);
        }
    }
}