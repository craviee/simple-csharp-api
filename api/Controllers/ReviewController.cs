using System.Collections.Generic;
using System.Linq;
using api.Dtos;
using api.Models;
using api.Services;
using Microsoft.AspNetCore.Mvc;

namespace api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReviewController : Controller
    {
        private readonly IReviewRepository _reviewRepository;
        private readonly IReviewerRepository _reviewerRepository;
        private readonly IBookRepository _bookRepository;

        public ReviewController(IReviewRepository reviewRepository, IReviewerRepository reviewerRepository, IBookRepository bookRepository)
        {
            _reviewRepository = reviewRepository;
            _reviewerRepository = reviewerRepository;
            _bookRepository = bookRepository;
        }

        [HttpGet]
        [ProducesResponseType(200, Type = typeof(IEnumerable<ReviewDto>))]
        [ProducesResponseType(400)]
        public IActionResult GetReviews()
        {
            IEnumerable<Review> reviews = _reviewRepository.GetReviews();

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            IEnumerable<ReviewDto> reviewsDto = reviews.Select(r => new ReviewDto(r.Id, r.ReviewText, r.Headline, r.Rating,
                new BookDto(r.Book.Id, r.Book.Title, r.Book.Isbn, r.Book.DatePublished,r.Book.BookAuthors.Select(b => new AuthorDto(b.AuthorId, b.Author.FirstName, b.Author.LastName, new CountryDto(b.Author.Country.Id, b.Author.Country.Name))).ToList(),
                    r.Book.BookCategories.Select(b => new CategoryDto(b.CategoryId, b.Category.Name)).ToList()),
                new ReviewerDto(r.Reviewer.Id, r.Reviewer.FirstName, r.Reviewer.LastName))).ToList();
            return Ok(reviewsDto);
        }

        [HttpGet("{reviewId:int}", Name = "GetReview")]
        [ProducesResponseType(200, Type = typeof(ReviewDto))]
        [ProducesResponseType(404)]
        [ProducesResponseType(400)]
        public IActionResult GetReview(int reviewId)
        {
            if (!_reviewRepository.ReviewExists(reviewId))
                return NotFound();

            var review = _reviewRepository.GetReview(reviewId);

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var reviewDto = new ReviewDto(review.Id, review.ReviewText, review.Headline, review.Rating,
                new BookDto(review.Book.Id, review.Book.Title, review.Book.Isbn, review.Book.DatePublished, review.Book.BookAuthors.Select(b => new AuthorDto(b.AuthorId, b.Author.FirstName, b.Author.LastName, new CountryDto(b.Author.Country.Id, b.Author.Country.Name))).ToList(),
                    review.Book.BookCategories.Select(b => new CategoryDto(b.CategoryId, b.Category.Name)).ToList()),
                new ReviewerDto(review.Reviewer.Id, review.Reviewer.FirstName, review.Reviewer.LastName));
            return Ok(reviewDto);
        }

        [HttpGet("reviewer/{reviewerId:int}")]
        [ProducesResponseType(200, Type = typeof(IEnumerable<ReviewDto>))]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public IActionResult GetReviewsByReviewer(int reviewerId)
        {
            if (!_reviewerRepository.ReviewerExistsById(reviewerId))
                return NotFound();

            var reviews = _reviewRepository.GetReviewsByReviewer(reviewerId);

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var reviewsDto = reviews.Select(r => new ReviewDto(r.Id, r.ReviewText, r.Headline, r.Rating,
                new BookDto(r.Book.Id, r.Book.Title, r.Book.Isbn, r.Book.DatePublished, r.Book.BookAuthors.Select(b => new AuthorDto(b.AuthorId, b.Author.FirstName, b.Author.LastName, new CountryDto(b.Author.Country.Id, b.Author.Country.Name))).ToList(),
                    r.Book.BookCategories.Select(b => new CategoryDto(b.CategoryId, b.Category.Name)).ToList()),
                new ReviewerDto(r.Reviewer.Id, r.Reviewer.FirstName, r.Reviewer.LastName)));
            return Ok(reviewsDto);
        }

        [HttpGet("book/{bookId:int}")]
        [ProducesResponseType(200, Type = typeof(IEnumerable<ReviewDto>))]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public IActionResult GetReviewsByBook(int bookId)
        {
            if (!_bookRepository.BookExistsById(bookId))
                return NotFound();

            var reviews = _reviewRepository.GetReviewsByBook(bookId);

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var reviewsDto = reviews.Select(r => new ReviewDto(r.Id, r.ReviewText, r.Headline, r.Rating,
                new BookDto(r.Book.Id, r.Book.Title, r.Book.Isbn, r.Book.DatePublished, r.Book.BookAuthors.Select(b => new AuthorDto(b.AuthorId, b.Author.FirstName, b.Author.LastName, new CountryDto(b.Author.Country.Id, b.Author.Country.Name))).ToList(),
                    r.Book.BookCategories.Select(b => new CategoryDto(b.CategoryId, b.Category.Name)).ToList()),
                new ReviewerDto(r.Reviewer.Id, r.Reviewer.FirstName, r.Reviewer.LastName)));
            return Ok(reviewsDto);
        }

        [HttpPost]
        [ProducesResponseType(200, Type = typeof(ReviewDto))]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public IActionResult CreateReview([FromBody] Review newReview)
        {
            if (newReview == null) return BadRequest(ModelState);
            if (newReview.Book == null)
            {
                ModelState.AddModelError("", "There is no Book assigned to this Review");
                return BadRequest(ModelState);
            }
            if (newReview.Reviewer == null)
            {
                ModelState.AddModelError("", "There is no Reviewer assigned to this Review");
                return BadRequest(ModelState);
            }
            if (!_bookRepository.BookExistsById(newReview.Book.Id))
            {
                ModelState.AddModelError("", $"There is no book with Id {newReview.Book.Id}");
                return NotFound(ModelState);
            }
            if (!_reviewerRepository.ReviewerExistsById(newReview.Reviewer.Id))
            {
                ModelState.AddModelError("", $"There is no reviewer with Id {newReview.Reviewer.Id}");
                return NotFound(ModelState);
            }

            newReview.Book = _bookRepository.GetBookById(newReview.Book.Id);
            newReview.Reviewer = _reviewerRepository.GetReviewer(newReview.Reviewer.Id);

            if (!ModelState.IsValid) return BadRequest(ModelState);

            newReview.Id = 0;

            if (_reviewRepository.CreateReview(newReview)) return CreatedAtRoute("GetReview", new {reviewId = newReview.Id}, newReview);
            ModelState.AddModelError("", $"Something went wrong on creating the review");
            return StatusCode(500, ModelState);
        }

        [HttpPut("{reviewId:int}")]
        [ProducesResponseType(200, Type = typeof(ReviewDto))]
        [ProducesResponseType(500)]
        [ProducesResponseType(404)]
        [ProducesResponseType(400)]
        public IActionResult UpdateReview(int reviewId, [FromBody] Review updatedReview)
        {
            if (updatedReview == null || reviewId != updatedReview.Id) return BadRequest(ModelState);
            if (!_reviewRepository.ReviewExists(reviewId)) return NotFound();
            if (updatedReview.Book == null)
            {
                ModelState.AddModelError("", "There is no Book assigned to this Review");
                return BadRequest(ModelState);
            }
            if (updatedReview.Reviewer == null)
            {
                ModelState.AddModelError("", "There is no Reviewer assigned to this Review");
                return BadRequest(ModelState);
            }
            if (!_bookRepository.BookExistsById(updatedReview.Book.Id))
            {
                ModelState.AddModelError("", $"There is no book with Id {updatedReview.Book.Id}");
                return NotFound(ModelState);
            }
            if (!_reviewerRepository.ReviewerExistsById(updatedReview.Reviewer.Id))
            {
                ModelState.AddModelError("", $"There is no reviewer with Id {updatedReview.Reviewer.Id}");
                return NotFound(ModelState);
            }
            updatedReview.Book = _bookRepository.GetBookById(updatedReview.Book.Id);
            updatedReview.Reviewer = _reviewerRepository.GetReviewer(updatedReview.Reviewer.Id);
            if (_reviewRepository.UpdateReview(updatedReview)) return CreatedAtRoute("GetReview", new {reviewId = reviewId}, updatedReview);
            ModelState.AddModelError("", $"Something went wrong on updating review with Id {reviewId}");
            return StatusCode(500, ModelState);
        }

        [HttpDelete("{reviewId:int}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(500)]
        [ProducesResponseType(400)]
        public IActionResult DeleteReview(int reviewId)
        {
            if (!_reviewRepository.ReviewExists(reviewId)) return NotFound(ModelState);
            if (!ModelState.IsValid) return BadRequest(ModelState);
            if (_reviewRepository.DeleteReview(reviewId)) return NoContent();
            ModelState.AddModelError("", $"Something went wrong on deleting review with Id {reviewId}");
            return StatusCode(500, ModelState);
        }
    }
}