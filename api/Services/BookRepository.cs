using System;
using System.Collections.Generic;
using System.Linq;
using api.Models;
using Microsoft.EntityFrameworkCore;

namespace api.Services
{
    public class BookRepository : IBookRepository
    {
        private readonly BookDbContext _bookContext;

        public BookRepository(BookDbContext bookContext)
        {
            _bookContext = bookContext;
        }

        public ICollection<Book> GetBooks()
        {
            return _bookContext.Books
                .Include(b => b.BookAuthors).ThenInclude(a => a.Author.Country)
                .Include(b => b.BookCategories).ThenInclude(c => c.Category)
                .OrderBy(b => b.Title).ToList();
        }

        public Book GetBookById(int bookId)
        {
            return _bookContext.Books
            .Include(b => b.BookAuthors).ThenInclude(a => a.Author.Country)
            .Include(b => b.BookCategories).ThenInclude(c => c.Category)
            .FirstOrDefault(b => b.Id == bookId);
        }

        public Book GetBookByIsbn(string bookIsbn)
        {
            return _bookContext.Books
            .Include(b => b.BookAuthors).ThenInclude(a => a.Author.Country)
            .Include(b => b.BookCategories).ThenInclude(c => c.Category)
            .FirstOrDefault(b => b.Isbn == bookIsbn);
        }

        public ICollection<Book> GetBooksOfAnAuthor(int authorId)
        {
            var bookIds = _bookContext.BookAuthors.Where(ba => ba.AuthorId == authorId)
            .Select(ba => ba.Book.Id).ToList();

            return _bookContext.Books.Where(b => bookIds.Contains(b.Id))
            .Include(b => b.BookAuthors).ThenInclude(a => a.Author.Country)
            .Include(b => b.BookCategories).ThenInclude(c => c.Category)
            .OrderBy(b => b.Title).ToList();;
        }

        public ICollection<Book> GetBooksByCategory(int categoryId)
        {
            var bookIds = _bookContext.BookCategories.Where(bc => bc.CategoryId == categoryId).Select(ba => ba.Book.Id).ToList();

            return _bookContext.Books.Where(b => bookIds.Contains(b.Id))
            .Include(b => b.BookAuthors).ThenInclude(a => a.Author.Country)
            .Include(b => b.BookCategories).ThenInclude(c => c.Category)
            .OrderBy(b => b.Title).ToList();
        }

        public Book GetBookByReview(int reviewId)
        {
            var bookIds = _bookContext.Reviews.Where(r => r.Id == reviewId).Select(r => r.Book.Id).ToList();
            return _bookContext.Books.Where(b => bookIds.Contains(b.Id))
            .Include(b => b.BookAuthors).ThenInclude(a => a.Author.Country)
            .Include(b => b.BookCategories).ThenInclude(c => c.Category)
            .FirstOrDefault();
        }

        public decimal GetAverageRatingByBook(int bookId)
        {
            var ratings = _bookContext.Reviews.Where(r => r.Book.Id == bookId).Select(r => r.Rating).ToList();
            return ratings.Count != 0 ? Convert.ToDecimal(ratings.Average()) : 0;
        }

        public bool BookExistsById(int bookId)
        {
            return _bookContext.Books.Any(b => b.Id == bookId);
        }
        public bool BookExistsByIsbn(string bookIsbn)
        {
            return _bookContext.Books.Any(b => b.Isbn == bookIsbn);
        }

        public bool BookDuplicatedByIsbn(int bookId, string isbn)
        {
            return _bookContext.Books.Any(b => b.Isbn == isbn && b.Id != bookId);
        }

        public bool CreateBook(List<int> authorIds, List<int> categoryIds, Book newBook)
        {

            var authors = _bookContext.Authors.Where(a => authorIds.Contains(a.Id)).ToList();
            var categories = _bookContext.Categories.Where(c => categoryIds.Contains(c.Id)).ToList();
            var bookCategories = categories.Select(c => new BookCategory() {Category = c, Book = newBook}).ToList();
            var bookAuthors = authors.Select(a => new BookAuthor() {Author = a, Book = newBook}).ToList();

            _bookContext.Books.AddAsync(newBook);
            _bookContext.BookAuthors.AddRangeAsync(bookAuthors);
            _bookContext.BookCategories.AddRangeAsync(bookCategories);

            return Save();
        }

        public bool UpdateBook(List<int> authorIds, List<int> categoryIds, Book updatedBook)
        {
            var authors = _bookContext.Authors.Where(a => authorIds.Contains(a.Id)).ToList();
            var categories = _bookContext.Categories.Where(c => categoryIds.Contains(c.Id)).ToList();
            var newBookCategories = categories.Select(c => new BookCategory() {Category = c, Book = updatedBook}).ToList();
            var newBookAuthors = authors.Select(a => new BookAuthor() {Author = a, Book = updatedBook}).ToList();
            var oldBookCategories = _bookContext.BookCategories.Where(bc => bc.BookId == updatedBook.Id).ToList();
            var oldBookAuthors = _bookContext.BookAuthors.Where(ba => ba.BookId == updatedBook.Id).ToList();

            _bookContext.BookAuthors.RemoveRange(oldBookAuthors);
            _bookContext.BookCategories.RemoveRange(oldBookCategories);
            _bookContext.Books.Update(updatedBook);
            _bookContext.BookAuthors.AddRangeAsync(newBookAuthors);
            _bookContext.BookCategories.AddRangeAsync(newBookCategories);

            return Save();
        }

        public bool DeleteBook(int bookId)
        {
            var oldBookCategories = _bookContext.BookCategories.Where(bc => bc.BookId == bookId).ToList();
            var oldBookAuthors = _bookContext.BookAuthors.Where(ba => ba.BookId == bookId).ToList();

            _bookContext.BookAuthors.RemoveRange(oldBookAuthors);
            _bookContext.BookCategories.RemoveRange(oldBookCategories);
            _bookContext.Books.Remove(GetBookById(bookId));

            return Save();
        }

        public bool Save()
        {
            return _bookContext.SaveChanges() >= 0;
        }
    }
}