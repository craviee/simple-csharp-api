using System;
using System.Collections.Generic;
using System.Linq;
using api.Models;
using Microsoft.EntityFrameworkCore;

namespace api.Services
{
    public class AuthorRepository : IAuthorRepository
    {
        private readonly BookDbContext _authorContext;

        public AuthorRepository(BookDbContext authorContext)
        {
            _authorContext = authorContext;
        }

        public ICollection<Author> GetAuthors()
        {
            return _authorContext.Authors.Include(a => a.Country).OrderBy(a => a.LastName).ToList();
        }

        public Author GetAuthor(int authorId)
        {
            return _authorContext.Authors.Include(a => a.Country).FirstOrDefault(a => a.Id == authorId);
        }

        public ICollection<Author> GetAuthorsOfABook(int bookId)
        {
            return _authorContext.BookAuthors.Where(ba => ba.BookId == bookId).Include(a => a.Author.Country)
                .Select(ba => ba.Author).OrderBy(a => a.LastName).ToList();
        }

        public ICollection<Author> GetAuthorsFromACountry(int countryId)
        {
            return _authorContext.Authors.Where(a => a.Country.Id == countryId).Include(a => a.Country).OrderBy(a => a.LastName).ToList();
        }

        public bool AuthorExistsById(int authorId)
        {
            return _authorContext.Authors.Any(a => a.Id == authorId);
        }

        public bool AuthorDuplicatedByName(int authorId, string firstName, string lastName)
        {
            return _authorContext.Authors.Any(a => a.FirstName == firstName && a.LastName == lastName && a.Id != authorId);
        }

        public bool AuthorExistsByName(string firstName, string lastName)
        {
            return _authorContext.Authors.Any(a => a.FirstName == firstName && a.LastName == lastName);
        }

        public bool CreateAuthor(Author newAuthor)
        {
            _authorContext.Authors.AddAsync(newAuthor);
            return Save();
        }

        public bool UpdateAuthor(Author updatedAuthor)
        {
            _authorContext.Authors.Update(updatedAuthor);
            return Save();
        }

        public bool DeleteAuthor(int authorId)
        {
            _authorContext.Authors.Remove(GetAuthor(authorId));
            return Save();
        }

        public bool Save()
        {
            return _authorContext.SaveChanges() >= 0;
        }
    }
}