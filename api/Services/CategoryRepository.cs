using System.Collections.Generic;
using System.Linq;
using api.Models;

namespace api.Services
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly BookDbContext _categoryContext;

        public CategoryRepository(BookDbContext categoryContext)
        {
            _categoryContext = categoryContext;
        }

        public ICollection<Category> GetCategories()
        {
            return _categoryContext.Categories.OrderBy(c => c.Name).ToList();
        }

        public Category GetCategory(int categoryId)
        {
            return _categoryContext.Categories.FirstOrDefault(c => c.Id == categoryId);
        }

        public ICollection<Category> GetCategoriesByBook(int bookId)
        {
            return _categoryContext.BookCategories.Where(bc => bc.BookId == bookId).Select(bc => bc.Category).OrderBy(c => c.Name).ToList();
        }

        public bool CategoryExistsById(int categoryId)
        {
            return _categoryContext.Categories.Any(c => c.Id == categoryId);
        }

        public bool CategoryExistsByName(string categoryName)
        {
            return _categoryContext.Categories.Any(c => c.Name == categoryName);
        }

        public bool CreateCategory(Category newCategory)
        {
            _categoryContext.Categories.AddAsync(newCategory);
            return Save();
        }

        public bool UpdateCategory(Category updatedCategory)
        {
            _categoryContext.Categories.Update(updatedCategory);
            return Save();
        }

        public bool DeleteCategory(int categoryId)
        {
            _categoryContext.Categories.Remove(_categoryContext.Categories.FirstOrDefault(c => c.Id == categoryId) ?? new Category());
            return Save();
        }

        public bool Save()
        {
            return _categoryContext.SaveChanges() >= 0;
        }
    }
}