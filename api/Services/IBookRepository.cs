using System.Collections.Generic;
using api.Models;

namespace api.Services
{
    public interface IBookRepository
    {
        ICollection<Book> GetBooks();
        Book GetBookById(int bookId);
        Book GetBookByIsbn(string bookIsbn);
        ICollection<Book> GetBooksOfAnAuthor(int authorId);
        ICollection<Book> GetBooksByCategory(int categoryId);
        Book GetBookByReview(int reviewId);
        decimal GetAverageRatingByBook(int bookId);
        bool BookExistsById(int bookId);
        bool BookExistsByIsbn(string bookIsbn);
        bool BookDuplicatedByIsbn(int bookId, string isbn);
        bool CreateBook(List<int> authorIds, List<int> categoryIds, Book newBook);
        bool UpdateBook(List<int> authorIds, List<int> categoryIds, Book updatedBook);
        bool DeleteBook(int bookId);
        bool Save();
    }
}