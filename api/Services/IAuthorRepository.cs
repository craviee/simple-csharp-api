using System.Collections.Generic;
using api.Models;

namespace api.Services
{
    public interface IAuthorRepository
    {
        ICollection<Author> GetAuthors();
        Author GetAuthor(int authorId);
        ICollection<Author> GetAuthorsOfABook(int bookId);
        ICollection<Author> GetAuthorsFromACountry(int countryId);
        bool AuthorExistsById(int authorId);
        bool AuthorDuplicatedByName(int authorId, string firstName, string lastName);
        bool AuthorExistsByName(string firstName, string lastName);
        bool CreateAuthor(Author newAuthor);
        bool UpdateAuthor(Author updatedAuthor);
        bool DeleteAuthor(int authorId);
        bool Save();
    }
}