using System.Collections.Generic;
using api.Models;

namespace api.Services
{
    public interface ICategoryRepository
    {
        ICollection<Category> GetCategories();
        Category GetCategory(int categoryId);
        ICollection<Category> GetCategoriesByBook(int bookId);
        bool CategoryExistsById(int categoryId);
        bool CategoryExistsByName(string categoryName);
        bool CreateCategory(Category newCategory);
        bool UpdateCategory(Category updatedCategory);
        bool DeleteCategory(int categoryId);
        bool Save();
    }
}