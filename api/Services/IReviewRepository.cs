using System.Collections.Generic;
using api.Models;

namespace api.Services
{
    public interface IReviewRepository
    {
        ICollection<Review> GetReviews();
        Review GetReview(int reviewId);
        ICollection<Review> GetReviewsByReviewer(int reviewerId);
        ICollection<Review> GetReviewsByBook(int bookId);
        bool ReviewExists(int reviewId);
        bool CreateReview(Review newReview);
        bool UpdateReview(Review updatedReview);
        bool DeleteReview(int reviewId);
        bool Save();
    }
}