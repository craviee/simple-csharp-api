using System.Collections.Generic;
using System.Linq;
using api.Models;

namespace api.Services
{
    public class CountryRepository : ICountryRepository
    {
        private readonly BookDbContext _countryContext;

        public CountryRepository(BookDbContext countryContext)
        {
            _countryContext = countryContext;
        }

        public ICollection<Country> GetCountries()
        {
            return _countryContext.Countries.OrderBy(c => c.Name).ToList();
        }

        public Country GetCountry(int countryId)
        {
            return _countryContext.Countries.FirstOrDefault(c => c.Id == countryId);
        }

        public Country GetCountryOfAnAuthor(int authorId)
        {
            return _countryContext.Authors.Where(a => a.Id == authorId).Select(a => a.Country).FirstOrDefault();
        }

        public bool CountryExistsById(int countryId)
        {
            return _countryContext.Countries.Any(c => c.Id == countryId);
        }

        public bool CountryExistsByName(string name)
        {
            return _countryContext.Countries.Any(c => c.Name == name);
        }

        public bool CreateCountry(Country newCountry)
        {
            _countryContext.Countries.AddAsync(newCountry);
            return Save();
        }

        public bool UpdateCountry(Country updatedCountry)
        {
            _countryContext.Countries.Update(updatedCountry);
            return Save();
        }

        public bool DeleteCountry(int countryId)
        {
            _countryContext.Countries.Remove(_countryContext.Countries.FirstOrDefault(c => c.Id == countryId) ?? new Country());
            return Save();
        }

        public bool Save()
        {
            return _countryContext.SaveChanges() >= 0;
        }
    }
}