using System.Collections.Generic;
using api.Models;

namespace api.Services
{
    public interface ICountryRepository
    {
        ICollection<Country> GetCountries();
        Country GetCountry(int countryId);
        Country GetCountryOfAnAuthor(int authorId);
        bool CountryExistsById(int countryId);
        bool CountryExistsByName(string name);
        bool CreateCountry(Country newCountry);
        bool UpdateCountry(Country updatedCountry);
        bool DeleteCountry(int countryId);
        bool Save();
    }
}