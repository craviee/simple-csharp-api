using System.Collections.Generic;
using System.Linq;
using api.Models;
using Microsoft.EntityFrameworkCore;

namespace api.Services
{
    public class ReviewRepository : IReviewRepository
    {
        private readonly BookDbContext _reviewContext;

        public ReviewRepository(BookDbContext reviewContext)
        {
            _reviewContext = reviewContext;
        }

        public ICollection<Review> GetReviews()
        {
            var reviews = _reviewContext.Reviews
                .Include(r => r.Book.BookAuthors).ThenInclude(ba => ba.Author.Country)
                .Include(r => r.Book.BookCategories).ThenInclude(bc => bc.Category)
                .Include(r => r.Reviewer)
                .OrderBy(r => r.Rating).ToList();
            foreach (var review in reviews)
            {
                review.Book.Reviews = null;
                review.Reviewer.Reviews = null;
            }
            return reviews;
        }

        public Review GetReview(int reviewId)
        {
            return _reviewContext.Reviews.Include(r => r.Book.BookAuthors).ThenInclude(ba => ba.Author.Country)
                .Include(r => r.Book.BookCategories).ThenInclude(bc => bc.Category).Include(r => r.Reviewer).FirstOrDefault(r => r.Id == reviewId);
        }

        public ICollection<Review> GetReviewsByReviewer(int reviewerId)
        {
            return _reviewContext.Reviews.Where(r => r.Reviewer.Id == reviewerId).Include(r => r.Book.BookAuthors).ThenInclude(ba => ba.Author.Country)
                .Include(r => r.Book.BookCategories).ThenInclude(bc => bc.Category).Include(r => r.Reviewer).OrderBy(r => r.Rating).ToList();
        }

        public ICollection<Review> GetReviewsByBook(int bookId)
        {
            return _reviewContext.Reviews.Where(r => r.Book.Id == bookId).Include(r => r.Book.BookAuthors).ThenInclude(ba => ba.Author.Country)
                .Include(r => r.Book.BookCategories).ThenInclude(bc => bc.Category).Include(r => r.Reviewer).OrderBy(r => r.Rating).ToList();
        }

        public bool ReviewExists(int reviewId)
        {
            return _reviewContext.Reviews.Any(r => r.Id == reviewId);
        }

        public bool CreateReview(Review newReview)
        {
            _reviewContext.Reviews.AddAsync(newReview);
            return Save();
        }

        public bool UpdateReview(Review updatedReview)
        {
            _reviewContext.Reviews.Update(updatedReview);
            return Save();
        }

        public bool DeleteReview(int reviewId)
        {
            _reviewContext.Reviews.Remove(GetReview(reviewId));
            return Save();
        }

        public bool Save()
        {
            return _reviewContext.SaveChanges() >= 0;
        }
    }
}