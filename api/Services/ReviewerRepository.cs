using System.Collections.Generic;
using System.Linq;
using api.Models;

namespace api.Services
{
    public class ReviewerRepository : IReviewerRepository
    {
        private readonly BookDbContext _reviewerContext;

        public ReviewerRepository(BookDbContext reviewerContext)
        {
            _reviewerContext = reviewerContext;
        }

        public ICollection<Reviewer> GetReviewers()
        {
            return _reviewerContext.Reviewers.OrderBy(r => r.LastName).ToList();
        }

        public Reviewer GetReviewer(int reviewerId)
        {
            return _reviewerContext.Reviewers.FirstOrDefault(r => r.Id == reviewerId);
        }

        public ICollection<Reviewer> GetReviewersByBook(int bookId)
        {
            var reviewsIds = _reviewerContext.Reviews.Where(r => r.Book.Id == bookId).Select(r => r.Id).ToList();
            return _reviewerContext.Reviews.Where(r => reviewsIds.Contains(r.Id)).Select(r => r.Reviewer).ToList();
        }

        public Reviewer GetReviewerByReview(int reviewId)
        {
            return _reviewerContext.Reviews.Where(r => r.Id == reviewId).Select(r => r.Reviewer).FirstOrDefault();
        }

        public bool ReviewerExistsById(int reviewerId)
        {
            return _reviewerContext.Reviewers.Any(r => r.Id == reviewerId);
        }

        public bool ReviewerExistsByName(string firstName, string lastName)
        {
            return _reviewerContext.Reviewers.Any(r => r.FirstName == firstName && r.LastName == lastName);
        }

        public bool CreateReviewer(Reviewer newReviewer)
        {
            _reviewerContext.Reviewers.AddAsync(newReviewer);
            return Save();
        }

        public bool UpdateReviewer(Reviewer updatedReviewer)
        {
            _reviewerContext.Reviewers.Update(updatedReviewer);
            return Save();
        }

        public bool DeleteReviewer(int reviewerId)
        {
            _reviewerContext.Reviewers.Remove(GetReviewer(reviewerId));
            return Save();
        }

        public bool Save()
        {
            return _reviewerContext.SaveChanges() >= 0;
        }
    }
}