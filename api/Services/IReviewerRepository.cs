using System.Collections.Generic;
using api.Models;

namespace api.Services
{
    public interface IReviewerRepository
    {
        ICollection<Reviewer> GetReviewers();
        Reviewer GetReviewer(int reviewerId);
        ICollection<Reviewer> GetReviewersByBook(int bookId);
        Reviewer GetReviewerByReview(int reviewId);
        bool ReviewerExistsById(int reviewerId);
        bool ReviewerExistsByName(string firstName, string lastName);
        bool CreateReviewer(Reviewer newReviewer);
        bool UpdateReviewer(Reviewer updatedReviewer);
        bool DeleteReviewer(int reviewerId);
        bool Save();
    }
}