using api.Models;

namespace api.Dtos
{
    public class AuthorDto
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public CountryDto Country { get; set; }

        public AuthorDto(int id, string firstName, string lastName, CountryDto country)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
            Country = country;
        }
    }
}