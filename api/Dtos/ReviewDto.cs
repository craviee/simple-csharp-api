using api.Models;

namespace api.Dtos
{
    public class ReviewDto
    {
        public int Id { get; set; }
        public string ReviewText { get; set; }
        public string Headline { get; set; }
        public int Rating { get; set; }
        public BookDto Book { get; set; }
        public ReviewerDto Reviewer { get; set; }

        public ReviewDto(int id, string reviewText, string headline, int rating, BookDto book, ReviewerDto reviewer)
        {
            Id = id;
            ReviewText = reviewText;
            Headline = headline;
            Rating = rating;
            Book = book;
            Reviewer = reviewer;
        }
    }
}