using System;
using System.Collections;
using System.Collections.Generic;

namespace api.Dtos
{
    public class BookDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string ISBN { get; set; }
        public DateTime? DatePublished { get; set; }
        public IEnumerable<AuthorDto> Authors;
        public IEnumerable<CategoryDto> Categories;

        public BookDto(int id, string title, string isbn, DateTime? datePublished, IEnumerable<AuthorDto> authors = null, IEnumerable<CategoryDto> categories = null)
        {
            Id = id;
            Title = title;
            ISBN = isbn;
            DatePublished = datePublished;
            Authors = authors;
            Categories = categories;
        }
    }
}